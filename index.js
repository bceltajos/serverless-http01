const serverless = require('serverless-http')
const express = require('express')
const app = express()
 
app.get('/', function (req, res) {
  res.send('Hello World!')
})
 
//module.exports.handler = serverless(app)

module.exports.handler = async (event, context, callback) => {
    // you can do other things here
    let result

    const timer = setTimeout(() => {
        console.log("oh no i'm going to timeout in 3 seconds!");
        // &c.
    }, context.getRemainingTimeInMillis() - 3 * 1000);
    try {
    // rest of code...
        result = await handler(event, context);
    } finally {
        clearTimeout(timer);
    }

    callback(null, result);

    //const result = await handler(event, context);
    // and here
    // return result;
};